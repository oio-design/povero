/*jshint -W033 */

var url = ""
var webview = document.getElementById('internet')

$("#address").keyup(function (e) {
    if (e.keyCode == 13) {
        url = $("#address").val()

        // https://www.google.ae/?hl=ar&q=benis
        if (url.indexOf(' ') >= 0 || url.indexOf('.') < 0) {
          url.replace(' ', '+')
          // url = 'https://www.google.ae/?hl=ar#q=' + url
          url = 'https://www.duckduckgo.com/?q=' + url
        } else if(!(/^http:\/\//.test(url))){
    			url = "http://" + url
        }
				webview.loadURL(url)
    }
});

$(document).keyup(function(event){
  //left
  if (event.keyCode == 37) {
    webview.goBack()
  } else if (event.keyCode == 39) { //right
    webview.goForward()
  }
});


$(document).mousemove(function(e){
  var mouseY = e.pageY;

  if(mouseY <= 30 ) {
    document.getElementById('address').className = "jimmy"
  } else {
    document.getElementById('address').className = ""
  }
});

webview.addEventListener("did-start-loading", function(){
  loader.start()
  $("#spinner").show()
})

webview.addEventListener("did-frame-finish-load", function(){
  loader.stop()
  $("#spinner").hide()  
})

var loader = new loader()

function loader() {
  var elem = document.getElementById('spinner')
  var states = ["|", "/", "-", "\\"]
  var counter = 0
  var timer

  this.start = function() {
      timer = setInterval(function() {
        elem.innerHTML = states[counter]
        if (counter == states.length - 1) {
            counter = 0
        }
        else{
        	counter++
        }
    }, 100)
  }

  this.stop = function() {
    clearInterval(timer);
  }
}
